import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import ('../views/Home.vue')
  }
]

const context = require.context(`@/views/experiments`, true, /\.vue$/i)
context.keys().forEach((key) => {

  let filename = key.split('./')[1]
  let name = filename.split('.vue')[0]
  routes.push({
    path: `/${name}`,
    name,
    component: () => import (`@/views/experiments/${filename}`)

  })
})

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
